/*
 ESP-NOW based sensor using a BME280 temperature/pressure/humidity sensor
 Sends readings every 15 minutes to a server with a fixed mac address
 It takes about 215 milliseconds to wakeup, send a reading and go back to sleep, 
 and it uses about 70 milliAmps while awake and about 25 microamps while sleeping, 
 so it should last for a good year even AAA alkaline batteries. 
 Anthony Elder
 License: Apache License v2
*/
#include <ESP8266WiFi.h>
extern "C" {
  #include <espnow.h>
}
// #include "SparkFunBME280.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// this is the MAC Address of the remote ESP server which receives these sensor readings
uint8_t remoteMac[] = {0x5E, 0xCF, 0x7F, 0x27, 0xB6, 0x8A}; // base addr for svachu

#define WIFI_CHANNEL 1
#define SLEEP_SECS 15 * 60 // 15 minutes
#define SEND_TIMEOUT 245  // 245 millis seconds timeout 
#define BME_POWER 14


void gotoSleep();
void readBME280();
void readBME280Ada();

// keep in sync with slave struct
struct __attribute__((packed)) SENSOR_DATA {
  char id[10];
  float temp;
  float pressure;
  float voltage;
  float illu;
  float humidity;
} sensorData;

//BME280 bme280;
Adafruit_BME280 bme; 

volatile boolean callbackCalled;

void setup() {
  Serial.begin(115200); Serial.println();

  pinMode(BME_POWER, OUTPUT);
  digitalWrite(BME_POWER, HIGH);

  // read sensor first before awake generates heat
  readBME280();
  readBME280Ada();

  WiFi.mode(WIFI_STA); // Station mode for esp-now sensor node
  WiFi.disconnect();

  Serial.printf("This mac: %s, ", WiFi.macAddress().c_str()); 
  Serial.printf("target mac: %02x%02x%02x%02x%02x%02x", remoteMac[0], remoteMac[1], remoteMac[2], remoteMac[3], remoteMac[4], remoteMac[5]); 
  Serial.printf(", channel: %i\n", WIFI_CHANNEL); 

  if (esp_now_init() != 0) {
    Serial.println("*** ESP_Now init failed");
    gotoSleep();
  }

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_add_peer(remoteMac, ESP_NOW_ROLE_SLAVE, WIFI_CHANNEL, NULL, 0);

  esp_now_register_send_cb([](uint8_t* mac, uint8_t sendStatus) {
    Serial.printf("send_cb, send done, status = %i\n", sendStatus);
    callbackCalled = true;
  });

  callbackCalled = false;

  uint8_t bs[sizeof(sensorData)];
  memcpy(bs, &sensorData, sizeof(sensorData));
  esp_now_send(NULL, bs, sizeof(sensorData)); // NULL means send to all peers
}

void loop() {
  if (callbackCalled || (millis() > SEND_TIMEOUT)) {
    gotoSleep();
  }
}

void readBME280() {
  // bme280.beginI2C();
  // bme280.settings.commInterface = I2C_MODE;
  // bme280.settings.I2CAddress = 0x76;
  // bme280.settings.runMode = 3; // Forced mode with deepSleep
  // bme280.settings.tempOverSample = 1;
  // bme280.settings.pressOverSample = 1;
  // bme280.settings.humidOverSample = 1;
  // Serial.print("bme280 init="); Serial.println(bme280.begin(), HEX);
  // sensorData.temp = bme280.readTempC();
  // sensorData.humidity = bme280.readFloatHumidity();
  // sensorData.pressure = bme280.readFloatPressure() / 100.0;
  // sensorData.voltage = ((((float)analogRead(A0)/1024-0.024)*570)/100);
  // String name = "piW_2";
  // name.toCharArray(sensorData.id, 10, 0);
  // Serial.printf("temp=%01f, humidity=%01f, pressure=%01f\n", sensorData.temp, sensorData.humidity, sensorData.pressure);
}

void readBME280Ada() {
  unsigned status;
  status = bme.begin();  
  bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X1, // temperature
                    Adafruit_BME280::SAMPLING_X1, // pressure
                    Adafruit_BME280::SAMPLING_X1, // humidity
                    Adafruit_BME280::FILTER_OFF   );

  if (!status) {
      Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
      Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(),16);
      Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
      Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
      Serial.print("        ID of 0x60 represents a BME 280.\n");
      Serial.print("        ID of 0x61 represents a BME 680.\n");
      while (1);
  }

  sensorData.temp = bme.readTemperature();
  sensorData.pressure = bme.readPressure() / 100.0F;
  sensorData.humidity = bme.readHumidity();
  sensorData.voltage = ((((double)analogRead(A0)/1024-0.024)*570)/100);
  String name = "$31";
  name.toCharArray(sensorData.id, 10, 0);
  Serial.printf("temp=%01f, humidity=%01f, pressure=%01f\n", sensorData.temp, sensorData.humidity, sensorData.pressure);

}

void gotoSleep() {
  // add some randomness to avoid collisions with multiple devices
  int sleepSecs = SLEEP_SECS + ((uint8_t)RANDOM_REG32/2); 

  sleepSecs = 60;
  Serial.printf("Up for %i ms, going to sleep for %i secs...\n", millis(), sleepSecs); 
  ESP.deepSleep(sleepSecs * 1000000, RF_NO_CAL);
}